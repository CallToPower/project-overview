# README

## Github

* CallToPower: https://github.com/CallToPower
* CallToPower Legacy: https://github.com/CallToPowerLegacy

## Workspaces

* CTP: https://bitbucket.org/CallToPower/
* COYO Projects: https://bitbucket.org/coyoprojects/
* Opencast Projects: https://bitbucket.org/opencastprojects/
* CTP Games: https://bitbucket.org/calltopowergames/
* CTP Legacy: https://bitbucket.org/calltopowerlegacy/
* Recordr: https://bitbucket.org/recordr/
* Zibetsa: https://bitbucket.org/zibetsa/
